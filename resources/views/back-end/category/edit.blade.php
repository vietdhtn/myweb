@extends('back-end.layouts.master')
@section('title','Thêm danh mục')
@section('NoiDung')
<div class="col-md-12">
  	<div class="row">      
      <div class="col-md-12" style=" padding:5px;">
      <div class="panel panel-primary">
      	<div class="panel-heading">
      		<h3 class="panel-title">Nhập thông tin danh mục mới</h3>
      	</div>
      	<div class="panel-body">
	      	<form action="" method="POST" role="form">
	      		<input type="hidden" name="_token" value="{{ csrf_token() }}">
	      		<div class="form-group">
		      		<label for="input-id">Danh mục cha</label>
		      		<select name="sltCate" id="inputSltCate" class="form-control">
		      			<option value="0">-- ROOT --</option>
		      			<?php MenuMulti($cha,0,$str='---| ',$data['parent_id']); ?>
		      		</select>
	      		</div>
	      		<div class="form-group">
	      			<label for="input-id">Tên danh mục</label>
	      			<input type="text" name="txtCateName" id="inputTxtCateName" class="form-control" value="{!! old('txtCateName', isset($data['c_name']) ? $data['c_name'] : null)!!}" required="required">
	      		</div>
	      		<div class="form-group">
	      			<label for="input-id">Ghi chú</label>
	      			<input type="text" name="txtNote" id="inputTxtNote" class="form-control" value="{!! old('txtNote', isset($data['note']) ? $data['note'] : null)!!}">
	      		</div>
	      		<div class="form-group">
		      		<label for="input-id">Trạng thái tài khoản</label>
		      		<select name="sltStatus" id="inputsltStatus" class="form-control">
		      			@if ($data['status'] == 0)
			      			<option value="1">-- Đang Hiển Thị --</option>	
			      			<option value="0" selected>-- Không Hiển Thị --</option>
		      			@else
			      			<option value="1" selected>-- Đang Hiển Thị --</option>	
			      			<option value="0" >-- Không Hiển Thị --</option>
		      			@endif	      			
		      		</select>
	      		</div>
	      		<input type="submit" name="btnCateAdd" class="btn btn-primary" value="Lưu Lại" class="button" />
	      	</form>			      				      		
      	</div>
      </div>
      </div>	     
  	</div>
</div>
@endsection