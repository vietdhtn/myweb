    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span> Về Đầu</a>
      <div class="container-fluid" id="coppy">     
        <div class="container">
          <p>
            &copy; Coppyright 2016. <a href="#" title="">Myweb</a>  <br>
            Vui lòng ghi rõ nguồn khi sử dụng bài viết của myweb
          </p>          
        </div>
      </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="{!!url('public/front-end/js/bootstrap.min.js')!!}"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{!!url('public/front-end/js/hover-menu.js')!!}"></script>
    <script src="{!!asset('public/back-end/js/jquery-3.1.0.min.js')!!}"></script>
    <script src="{!!asset('public/back-end/js/myscrip.js')!!}"></script>
  </body>
</html>