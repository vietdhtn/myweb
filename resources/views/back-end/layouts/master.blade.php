@include ('back-end/layouts/header')
@include ('back-end/modules/menu')
{{-- container --}}
<div class="container">
	@include('back-end.blocks.error')
    @include('back-end.blocks.flash')
 	@yield('NoiDung') 
 </div><!-- /.container -->
@include ('back-end/layouts/footer')