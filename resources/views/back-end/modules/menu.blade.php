
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{!!route('home')!!}">Admin Pages</a>
    </div>
    <div id="navbar" class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="{!!route('home')!!}">Home</a></li>
        <li><a href="{!!route('listCate')!!}">Danh mục</a></li>        
        <li><a href="{!!route('listNews')!!}">Tin Tức</a></li>
        <li><a href="{!!route('listUser')!!}#contact">Thành Viên</a></li>
        <li><a href="#about">Banner - Quảng Cáo</a></li>
      </ul>
      <ul class="nav navbar-nav pull-right">
        <li><a href="#" style="color:green;">Xin Chào : Admin </a></li>
        <li><a href="{!!route('getlogout')!!}" style="color:red;">Thoát</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>