
@if (count($errors) > 0)
<ul class="error_msg" style="color:red;">
    @foreach ($errors->all() as $error)
        <li style="list-style:none; font-size:22px;">{{ $error }}</li>
    @endforeach
</ul>
@endif