<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="QuocTuan.Info" />
    <link rel="stylesheet" href="{!!asset('public/back-end/css/login.css')!!}" >
    <link rel="stylesheet" href="{!!asset('public/front-end/css/bootstrap.min.css')!!}" >
	<title>Khu vực quản trị :: Đăng nhập</title>
</head>
<body>
<div class="container">
    <div id="main">   
        {{-- @include('admin.blocks.error')    --}}  
        <form action="" method="POST" >
            <fieldset>
                <legend class="text-center label-success" style="color:#FFFFFF;" >Thông Tin Đăng Nhập</legend>               
                <table>
                    <tr>
                        <td class="login_img"></td>
                        <td>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <span class="label label-info">Email :</span>
                            <span class="form_item">
                                <input type="text" name="txtEmail" class="form-control" placeholder="Nhập email " />
                            </span><br />
                            <span class="label label-info">Password:</span>
                            <span class="form_item">
                                <input type="password" name="txtPass" class="form-control"  placeholder="Nhập mật khẩu" />
                            </span><br />                            
                            <span class="label label-info"></span>
                            <span class="form_item">
                                <input type="submit" name="btnLogin" value="Đăng nhập" class="btn btn-primary" />
                            </span>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </form>
        <hr>
        <footer>
           <p class="text-center"> Coppy right ...</p>
        </footer>
    </div>
</div>
<script src="{!!asset('public/back-end/js/jquery-3.1.0.min.js')!!}"></script>
<script src="{!!asset('public/back-end/js/myscrip.js')!!}"></script>
</body>
</html>