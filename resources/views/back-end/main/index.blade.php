@extends('back-end.layouts.master')
	{{-- main content --}}
	@section('NoiDung')
		<h2><strong> KHU VỰC QUẢN TRỊ DUNG WEBSITE</strong></h2>
		<hr>
      	<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
           	<div class="thumbnail">
           		<a href="{!!route('listCate')!!}" title=""><img src="{!!url('public/back-end/images/category.png')!!}" alt="" width="100px" height="100px"></a>
           		<div class="caption">
           			<h3 class="text-center"> Quản Lý Danh mục</h3>
           			<p>
           				Quản danh mục tin tức : Thêm - Xóa - Sửa các bản tin
           			</p>
           			<p>
           				Tổng số tin: <strong>1024</strong>         				
           			</p>
           		</div>
           	</div>           	
        </div>   
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
           	<div class="thumbnail">
           		<a href="{!!route('listNews')!!}" title=""><img src="{!!url('public/back-end/images/news.png')!!}" alt="" width="100px" height="100px"></a>
           		<div class="caption">
           			<h3 class="text-center"> Quản Lý Tin Tức</h3>
           			<p>
           				Quản lý tin tức : Thêm - Xóa - Sửa các bản tin
           			</p>
           			<p>
           				Tổng số tin: <strong>1024</strong>         				
           			</p>
           		</div>
           	</div>           	
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
           	<div class="thumbnail">
           		<a href="#" title=""><img src="{!!url('public/back-end/images/people.png')!!}" alt="" width="100px" height="100px"></a>
           		<div class="caption">
           			<h3 class="text-center"> Quản Lý Thành Viên</h3>
           			<p>
           				Quản lý Thành Viên : Thêm - Xóa - Sửa các bản tin
           			</p>
           			<p>
           				Tổng số tin: <strong>1024</strong>         				
           			</p>
           		</div>
           	</div>           	
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
           	<div class="thumbnail">
           		<a href="#" title=""><img src="{!!url('public/back-end/images/comments.png')!!}" alt="" width="100px" height="100px"></a>
           		<div class="caption">
           			<h3 class="text-center"> Quản Lý Tin Nhắn</h3>
           			<p>
           				Quản lý Tin Nhắn : Thêm - Xóa - Sửa các bản tin
           			</p>
           			<p>
           				Tổng số tin: <strong>1024</strong>         				
           			</p>
           		</div>
           	</div>           	
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
           	<div class="thumbnail">
           		<a href="#" title=""><img src="{!!url('public/back-end/images/comment.png')!!}" alt="" width="100px" height="100px"></a>
           		<div class="caption">
           			<h3 class="text-center"> Quản Lý Comment </h3>
           			<p>
           				Quản lý Các Bài trả lời : Thêm - Xóa - Sửa các bản tin
           			</p>
           			<p>
           				Tổng số tin: <strong>1024</strong>         				
           			</p>
           		</div>
           	</div>           	
        </div>
         <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
           	<div class="thumbnail">
           		<a href="#" title=""><img src="{!!url('public/back-end/images/ads.png')!!}" alt="" width="100px" height="100px"></a>
           		<div class="caption">
           			<h3 class="text-center"> Quản Lý Quảng Cáo </h3>
           			<p>
           				Quản lý Các quản cáo : Thêm - Xóa - Sửa các bản tin
           			</p>
           			<p>
           				Tổng số tin: <strong>1024</strong>         				
           			</p>
           		</div>
           	</div>           	
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
           	<div class="thumbnail">           		
           		<div class="caption">
           			<h3> Thống Kê Truy Cập</h3>
           			<p style="float: left;">
           				<img src="{!!url('public/back-end/images/report.png')!!}" alt="" width="150px" height="150px">
           			</p>
           			<p>
           				ượi ghé thăm trong ngày: <strong>1024</strong>  <br>
           				Tổng lượi ghé thăm: <strong>1024</strong>  <br>
           				Tổng số thành viên: <strong>1024</strong>   <br>
           				Tổng số bài viết: <strong>1024</strong>  <br>
           			</p>
           		</div>
           	</div>           	
        </div>  
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
           	<div class="thumbnail">           		
           		<div class="caption">
           			<h3> Thống Kê Nội Dung</h3>
           			<p style="float: left;">
           				<img src="{!!url('public/back-end/images/report.png')!!}" alt="" width="150px" height="150px">
           			</p>
           			<p>
           				Tổng chuyên mục: <strong>1024</strong>  <br>
           				Tổng bài viết: <strong>1024</strong>  <br>
           				Tổng số thành viên: <strong>1024</strong>   <br>
           				Thành Viên đang Online : <strong>124</strong>  <br>
           			</p>
           		</div>
           	</div>           	
        </div> 
    @endsection{{-- /main content --}}