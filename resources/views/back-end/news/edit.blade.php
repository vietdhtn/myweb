@extends('back-end.layouts.master')
@section('title','Thêm user')
@section('NoiDung')
<div class="col-md-12">
<div class="row">      
    <div class="col-md-12" style=" padding:5px;">
	    <div class="panel panel-primary">
	      	<div class="panel-heading">
	      		<h3 class="panel-title">Nhập thông tin danh mục mới</h3>
	      	</div>
	      	<div class="panel-body">
				<form action="" method="POST" enctype="multipart/form-data">
					<fieldset>
						<legend>Thông Tin Bản Tin</legend>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<span class="form_label">Tên danh mục:</span>
						<span class="form_item">
							<select name="sltCate"  class="form-control">
								<option value="">Chọn danh mục</option>
								<?php menuMulti ($danhmuc,0,$str="---|",$data["parent_id"]); ?>
							</select>
						</span><br />
						<span class="form_label">Tiêu đề tin:</span>
						<span class="form_item">
							<input type="text" name="txtTitle" class="form-control" value="{!! old('txtTitle',isset($data["n_title"]) ? $data["n_title"] : null) !!}" />
						</span><br />
						<span class="form_label">Tác giả:</span>
						<span class="form_item">
							<input type="text" name="txtAuthor" class="form-control" value="{!! old('txtAuthor',isset($data["author"]) ? $data["author"] : null) !!}" />
						</span><br />
						<span class="form_label">Trích dẫn:</span>
						<span class="form_item">
							<textarea name="txtIntro" rows="5" class="textbox">{!! old('txtIntro',isset($data["intro"]) ? $data["intro"] : null) !!}</textarea>
							<script type="text/javascript">
								var editor = CKEDITOR.replace('txtIntro',{
									language:'vi',
									filebrowserImageBrowseUrl : '../../public/plugin/ckfinder/ckfinder.html?Type=Images',
									filebrowserFlashBrowseUrl : '../../public/plugin/ckfinder/ckfinder.html?Type=Flash',
									filebrowserImageUploadUrl : '../../plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
									filebrowserFlashUploadUrl : '../../public/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
								});
							</script>
						</span><br />
						<span class="form_label">Nội dung tin:</span>
						<span class="form_item">
							<textarea name="txtFull" rows="8" class="textbox">{!! old('txtFull',isset($data["full"]) ? $data["full"] : null) !!}</textarea>
							<script type="text/javascript">
								var editor = CKEDITOR.replace('txtFull',{
									language:'vi',
									filebrowserImageBrowseUrl : '../../public/plugin/ckfinder/ckfinder.html?Type=Images',
									filebrowserFlashBrowseUrl : '../../public/plugin/ckfinder/ckfinder.html?Type=Flash',
									filebrowserImageUploadUrl : '../../plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
									filebrowserFlashUploadUrl : '../../public/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
								});
							</script>
						</span><br />
						<span class="form_label">Hình hiện tại:</span>
						<span class="form_item">
							<img src="{!! isset($data["image"]) ? asset('public/uploads/news/'.$data["image"]) : asset('public/hv_admin/images/nophoto.png') !!}" width="100px" />
						</span><br />
						<span class="form_label">Hình đại diện:</span>
						<span class="form_item">
							<input type="file" name="newsImg" class="textbox" />
						</span><br />
						<div class="form-group">
		      				<label for="">Nguồn :</label>
		      				<input type="text" required name="txtSource" class="form-control" value="{!! old('txtSource',isset($data["source"]) ? $data["source"] : null) !!}"> 
	      				</div>
						<span class="form_label">Công bố tin:</span>
						<span class="form_item">
							<input type="radio" name="rdoPublic" value="1" 
							@if ($data["status"] == 1)
								checked 
							@endif
							 /> Có 
							<input type="radio" name="rdoPublic" value="0"
							@if ($data["status"] == 0)
								checked 
							@endif
							 /> Không
						</span><br />
						<span class="form_label"></span>
						<span class="form_item">
							<input type="submit" name="btnNewsEdit" value="Lưu lại" class="button btn btn-primary">
						</span>
					</fieldset>
				</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection