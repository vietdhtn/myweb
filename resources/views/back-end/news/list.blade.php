@extends('back-end.layouts.master')
@section('title','Tin Tức')
@section('NoiDung')
<div class="col-md-12">
    <div class="row">      
        <div class="col-md-12" style=" padding:5px;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Quản lý tin tức</h3>
                </div>
                <div class="panel-body">
                    <a class="button btn btn-primary" style="margin-bottom:5px;" href="{!!Route('getAddNews')!!}"> Thêm Tin</a> 
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Hình ảnh</td>
                                <td>Tiêu Đề</td>
                                <td>Tác Giả</td>
                                <td>Trạng thái</td>
                                <td>Thời Gian</td>
                                <td class="action_col">Thao tác</td>
                            </tr>
                        </thead>
                        <tbody>
                                @foreach($data['data'] as $row)
                            <tr>
                                <td class="aligncenter">{!!$row['id']!!}</td>
                                <td style="text-align: center; padding: 0;"> 
                                    <img src="{!!url('public/uploads/news/'.$row['image'])!!}" width="100" height="70" alt=""> 
                                </td>
                                <td>
                                    {!!$row['n_title']!!}
                                </td>
                                <td class="list_td aligncenter">{!!$row['author']!!}</td>
                                @if($row['status']=='1')
                                    <td class="list_td aligncenter" style="color:blue;">Đã hiện</td>
                                @else
                                <td class="list_td aligncenter" style="color:red;">Đã ẩn</td>
                                @endif
                                <?php \Carbon\Carbon::setlocale('vi'); ?>
                                <td style="text-align: center;">
                                    {!!\Carbon\Carbon::createFromTimestamp(strtotime($row['created_at']))->diffForHumans()!!} <br>
                                    {!!$row['created_at']!!}
                                </td>
                                <td style="width: 105px; text-align: center;">
                                    <a href="../news/edit/{!!$row['id']!!}"><span class="glyphicon glyphicon-edit" style="font-size: 22px; color:green;"></span> </a>&nbsp;&nbsp;
                                    <a href="../news/del/{!!$row['id']!!}" onclick="return xacnhan('Xác nhận xóa mục này ?')" ><span class="glyphicon glyphicon-remove" style="font-size: 22px; color:red;"></span></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>                        
                    </table>
                <div style="padding-right:25px; float:right;" >
                    {!!$page->render()!!}
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection