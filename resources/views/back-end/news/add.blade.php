@extends('back-end.layouts.master')
@section('title','Thêm Tin mới')
@section('NoiDung')
<div class="col-md-12">
  	<div class="row">      
      <div class="col-md-12" style=" padding:5px;">
	      <div class="panel panel-primary">
	      	<div class="panel-heading">
	      		<h3 class="panel-title">Nhập bản tin mới</h3>
	      	</div>
	      	<div class="panel-body">
	      		<form action="" method="POST" enctype="multipart/form-data" role="form">
	      			<legend>Thêm bản tin mới</legend>
	      			<input type="hidden" name="_token" value="{{ csrf_token() }}">
	      			<div class="form-group">
	      				<label for="">Tên danh mục</label> <br>
	      				<select name="sltCate" value="" class="form-control" required>
	      					<option value="">Chọn danh mục</option>
								<?php menuMulti ($danhmuc,0,$str="---|",old('sltCate')); 
								?>	
	      				</select>
	      			</div>
	      			<div class="form-group">
	      				<label for="">Tiêu đề</label>
	      				<input type="text" required name="txtTitle" class="form-control" value="{!! old('txtTitle') !!}" placeholder=""> 
	      			</div>
	      			<div class="form-group">
	      				<label for="">Tác giả</label>
	      				<input type="text" required name="txtAuthor" class="form-control" value="{!! old('txtAuthor') !!}" placeholder=""> 
	      			</div>
	      			<div class="form-group">
	      				<label for="">Trích dẫn</label>
	      				<textarea name="txtIntro" id="inputTxtIntro" class="form-control" rows="5" required="required">{!! old('txtIntro') !!}</textarea>
	      				<script type="text/javascript">
							var editor = CKEDITOR.replace('txtIntro',{
								language:'vi',
								filebrowserImageBrowseUrl : '../../public/plugin/ckfinder/ckfinder.html?Type=Images',
								filebrowserFlashBrowseUrl : '../../public/plugin/ckfinder/ckfinder.html?Type=Flash',
								filebrowserImageUploadUrl : '../../plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
								filebrowserFlashUploadUrl : '../../public/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
							});
						</script>
	      			</div>
	      			<div class="form-group">
	      				<label for="">Nội dung tin</label>
	      				<textarea name="txtFull" id="inputTxtIntro" class="form-control" rows="5" required="required">{!! old('txtFull') !!}</textarea>
	      				<script type="text/javascript">
							var editor = CKEDITOR.replace('txtFull',{
								language:'vi',
								filebrowserImageBrowseUrl : '../../public/plugin/ckfinder/ckfinder.html?Type=Images',
								filebrowserFlashBrowseUrl : '../../public/plugin/ckfinder/ckfinder.html?Type=Flash',
								filebrowserImageUploadUrl : '../../plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
								filebrowserFlashUploadUrl : '../../public/plugin/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
							});
						</script>
	      			</div>
	      			<div class="form-group">
	      				<label for="">Hình ảnh</label>
	      				<input type="file" name="newsImg" class="textbox" />
	      			</div>
	      			<div class="form-group">
	      				<label for="">Nguồn :</label>
	      				<input type="text" required name="txtSource" class="form-control" value="{!! old('txtSource') !!}" placeholder=""> 
	      			</div>
		      		<div class="form-group">
	      				<label for="">Trạng thái tin</label><br>
	      				<input type="radio" name="rdoPublic" value="1" checked="checked" >
							@if (old('rdoPublic') == "1")
								checked
							@endif
							 Công bố 
							<input type="radio" name="rdoPublic" value="0" > 
							@if (old('rdoPublic') == "0")
								checked
							@endif
							Không công bố
		      		</div>
					<input type="submit" name="btnNewsAdd" value="Thêm tin" class="button btn btn-primary" >
	      		</form>
	      	</div>
	      </div>
  		</div>
	</div>
</div>
@endsection