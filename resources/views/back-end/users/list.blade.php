@extends('admin.master')
@section('title','Thêm user')
@section('NoiDung')
<div class="col-md-12">
    <div class="row">      
      <div class="col-md-12" style=" padding:5px;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Danh sách users</h3>
                </div>
                <div class="panel-body">
                 <a class="button btn btn-primary" style="margin-bottom:5px;" href="{!!Route('getadduser')!!}"> Thêm Mới</a> 
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Họ Tên</th>
                                <th>Username</th>
                                <th>Quyền</th>
                                <th>Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $stt=0; ?>
                        @foreach ($data as $row)
                        <?php $stt=$stt+1; ?>
                        <tr >
                            <?php if($row['level']==1) {$lv='Admin';} else $lv='Member';?>
                            <td >{!! $stt !!}</td>
                             <td >{!! $row['name'] !!}</td>
                            <td>{!! $row['username'] !!}</td>
                            <td >
                                @if($row['level']==1)
                                    <span style="color: red; font-weight: bold;">{!! $lv !!}</span>
                                @else
                                    <span style="color: black; font-weight: bold;">{!! $lv !!}</span>
                                @endif
                            </td>
                            <td>
                                <a href="../user/edit/{!! $row['id'] !!}"><img src="{!!url('public/images/edit.png')!!}" /></a>&nbsp;&nbsp;&nbsp;
                                <a href="../user/del/{!! $row['id'] !!}"onclick="return xacnhan('Bạn có chắc xóa mục này?')" ><img src="{!!url('public/images/delete.png')!!}"/></a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
