@extends('admin.master')
@section('title','Thêm user')
@section('NoiDung')
<div class="col-md-12">
  	<div class="row">      
      <div class="col-md-12" style=" padding:5px;">
	      	<div class="panel panel-primary">
		      	<div class="panel-heading">
		      		<h3 class="panel-title">Sửa Thông tin user</h3>
		      	</div>
		      	<div class="panel-body">
					<form action="" method="POST" role="form">
						<legend>Form title</legend>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-group">
							<label for="">Tên người dùng</label>
							<input type="text" name="txtName" class="form-control" value="{!! old('txtName')!!}">
						</div>
						<div class="form-group">
							<label for="">Username</label>
							<input type="text" name="txtUser" class="form-control" value="{!! old('txtUser')!!}" >
						</div>
						<div class="form-group">
							<label for="">Password</label>
							<input type="password" name="txtPass" class="form-control" placeholder="Nhập mật khẩu"  >
						</div>
						<div class="form-group">
							<label for="">Confirm password</label>
							<input type="password" name="txtRepass" class="form-control" placeholder="Nhập lại mật khẩu"  >
						</div>
						<div class="form-group">
							<label for="">Quyền</label>
							<input type="radio" name="rdoLevel" value="1"
								@if (old('rdoLevel')==1)
									checked
								@endif
								 > Admin 
								<input type="radio" name="rdoLevel" value="0" 
								@if (old('rdoLevel')==0)
									checked
								@endif
								 > Member
						</div>
						<input type="submit" name="btnUserEdit" value="Thêm" class="btn btn-primary" >
					</form>
				</div>
			</div>
		</div>
	</div>
</div>  
@endsection 


