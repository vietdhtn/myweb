@extends('front-end.layouts.master')
@section('NoiDung')
      <div class="panel panel-success">
        <div class="panel-heading">
          <h3 class="panel-title"><span class="glyphicon glyphicon-home"><a href="{!!url('')!!}" title=""> Home</a></span> <span class="glyphicon glyphicon-chevron-right" style="font-size: 11px;"></span> <a href="#" title=""> {!!$cat!!}</a> 
            <span class="glyphicon glyphicon-chevron-right" style="font-size: 11px;"></span> <a href="#" title=""> {!!$scat!!}</a>           
          </h3>
        </div>
        @if($data_cat)
        <div class="panel-body">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="thumbnail">                   
                <div class="caption">
                  <img src="{!!url('public/uploads/news/'.$data[0]['image'])!!}" alt="" width="300" height="210" style="float: left; padding: 10px;">                            
                  <p class="text-left" style="padding: 2px;">
                    <h3 ><a href="{!!url('tin-tuc/chi-tiet/'.$data[0]['id'].'-'.$data[0]['n_slug'])!!}" title=""> {!!$data[0]['n_title']!!}</a></h3>
                      <small>Ngày đăng: <a href="" title="">{!!$data[0]['created_at']!!}</a> </small> <br>
                      <small>Người đăng: <a href="#" title="">{!!$data[0]['author']!!} </a></small> <br>
                      {!!$data[0]['intro']!!}                       
                    <a href="{!!url('tin-tuc/chi-tiet/'.$data[0]['id'].'-'.$data[0]['n_slug'])!!}" class="btn btn-success">Chi Tiết</a>
                  </p>
                </div>
              </div>
            </div>
            @foreach($data_cat as $row)
              <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                <div class="thumbnail no-padding" style="min-height: 300px;">                   
                  <div class="caption">
                  <h3 style="font-size: 14px; padding: 0;"><a href="{!!url('tin-tuc/chi-tiet/'.$row['id'].'-'.$row['n_slug'])!!}" title=""> {!!$row['n_title']!!}</a></h3> 
                    <small>Ngày đăng: <a href="" title="">{!!$row['created_at']!!}</a> </small> <br>
                    <small>Người đăng: <a href="#" title="">{!!$row['author']!!} </a></small> <br>
                    <img src="{!!url('public/uploads/news/'.$row['image'])!!}" alt="" width="150" height="130" style="float: left; padding: 8px;">                            
                    <p class="text-left" style="padding: 2px;">                                               
                      {!!$row['intro']!!}
                      <a href="{!!url('tin-tuc/chi-tiet/'.$row['id'].'-'.$row['n_slug'])!!}" class="btn btn-success">Chi Tiết</a>
                    </p>
                  </div>
                </div>
              </div>
            @endforeach            
            <ul class="pagination">
              <li><a href="#">&laquo;</a></li>
              <li><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">&raquo;</a></li>
            </ul>
          </div>
        </div>
        @else
          <div class="panel-body">
            <div class="row">
              <h1>Chưa có bài viết nào trong mục này !</h1>
            </div>
          </div>
        @endif
      </div>        
    {{-- include tab ghost  --}}
     @include('front-end.modules.right-news-web-tab')    
@endsection