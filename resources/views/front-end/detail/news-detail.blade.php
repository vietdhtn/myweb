@extends('front-end.layouts.master')
@section('NoiDung')
      <div class="panel panel-success">
        <div class="panel-heading">
          <h3 class="panel-title">
            <span class="glyphicon glyphicon-home"><a href="{!!url('')!!}" title=""> Home</a></span>
            <span class="glyphicon glyphicon-chevron-right" style="font-size: 11px;"></span> 
            <a href="#" title="">{!!$cat[0]['c_name']!!}</a> 
            <span class="glyphicon glyphicon-chevron-right" style="font-size: 11px;"></span> 
            <a style="font-size: 13px; color: #d35400;">{!!$data[0]['n_title']!!}</a>                
          </h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <h3 ><a title=""> {!!$data[0]['n_title']!!}</a></h3>                                                
              <small>Ngày đăng: <a href="" title="">1{!!$data[0]['created_at']!!}</a> </small> <br>
              <small>Người đăng: <a href="#" title="">{!!$data[0]['author']!!} </a></small> <br>
                  <p>
                    <img src="{!!url('public/uploads/news/'.$data[0]['image'])!!}" alt="" width="90%" style="display: block; margin-left: auto; margin-right: auto; border-radius: 10px ; ">  
                  </p>
                  <p class="noidung">                                                   
                    {!!$data[0]['full']!!}
                  </p>
                  <p style="color:green;">
                    <strong>Nguồn : {!!$data[0]['source']!!}</strong>
                  </p>
            </div>
          </div>
        </div>
      </div>        
  
    <!--  penel  2 tab page 4-->
    @include('front-end.modules.right-download-tab') 
@endsection