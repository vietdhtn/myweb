<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title> MyWeb template :  Học thiết kế web </title>

    <!-- Bootstrap core CSS -->
    <link href="{!!url('public/front-end/css/bootstrap.min.css')!!}" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="public/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{!!url('public/front-end/css/style.css')!!}" rel="stylesheet">
    <link href="{!!url('public/front-end/css/loginform.css')!!}" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="public/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container-fluid">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="row">
              <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
                <img src="{!!url('public/front-end/logo/toplogo.gif')!!}" alt="" width="90" height="90">
              </div>
              <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                <h2  class="text-center"><b>WellCome To MyWeb</b></h2>
                <p class="text-center"><smaill> Nơi Học tập - Chia sẻ - Thành công</smaill></p>
              </div>
            </div>             
          </div>
          <div class="col-lg-6">
              <div class="search">  
                <div class="form-group">        
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <form action="#" method="POST" class="form-inline" role="form">                                  
                        <input type="text" class="form-control" required="required" id="" placeholder="Nhập nội dung ">
                        <button type="submit" class="btn btn-primary">Tìm kiếm</button>
                      </form>
                      <div class="col-lg-12">
                        <p class="login text-right"> 
                        Để tham gia thảo luận bạn vui lòng  
                        <a href="" data-toggle="modal" data-target="#myModal" style="color: blue;">Đăng nhập</a>
                      </p>
                      </div> 
                    </div>         
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
