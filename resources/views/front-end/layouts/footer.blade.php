     <!--  =================footer================ -->
      <div class="container-fluid" id="foot">
        <div class="container">
          <div class="row">
            <div class="col-lg-4">               
                <ul class="menu">
                 <h2 class="text-left">Học tập</h2>
                  <li><a href="#">HTML5 - CSS3</a></li>                  
                  <li><a href="#">Bootstrap 3</a></li>                  
                  <li><a href="#">PHP</a></li>                  
                  <li><a href="#">Laravel 5</a></li>                  
                  <li><a href="#">WordPress</a></li>                  
                  <li><a href="#">Joomla</a></li>
                </ul>
            </div>            
            <div class="col-lg-4"> 
                <ul class="menu">
                <h2 class="text-left">Dịch Vụ</h2>
                  <li><a href="#">Website Bán Hàng</a></li>                  
                  <li><a href="#">Website Cty - D-nghiệp</a></li>                  
                  <li><a href="#"> Web Forum - Tmdt</a></li>                  
                  <li><a href="#">Website Tin Tức</a></li>                  
                  <li><a href="#">Website Cá Nhân</a></li>                  
                  <li><a href="#">Dịch vụ hosting - Doman</a></li>
                </ul>
            </div>
            <div class="col-lg-4">                
                <ul class="menu">
                  <h2 class="text-left">Download</h2>
                  <li><a href="#">Windows ISO </a></li>                  
                  <li><a href="#">Phần mền</a></li>
                  <li><a href="#">Games - Trò chơi</a></li>                  
                  <li><a href="#">Công cụ hệ thống</a></li>                  
                  <li><a href="#">Driver máy tính</a></li>                  
                  <li><a href="#">Hình nền máy tính</a></li>
                </ul>
            </div>
          </div>
        </div>
      </div> <!-- / footer-->
      <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span> Về Đầu</a>
      <div class="container-fluid" id="coppy">     
        <div class="container">
          <p>
            &copy; Coppyright 2016. <a href="#" title="">Myweb</a>  <br>
            Vui lòng ghi rõ nguồn khi sử dụng bài viết của myweb
          </p>          
        </div>
      </div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="{!!url('public/front-end/js/bootstrap.min.js')!!}"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{!!url('public/front-end/js/hover-menu.js')!!}"></script>
  </body>
</html>