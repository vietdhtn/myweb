@include ('front-end/layouts/header')
    <!--  container main content  -->
@include('front-end.modules.login-form')
    {{--=========== include menu top=================== --}}
    @include('front-end.modules.main-top-menu')     
    <div class="container">
      <div class="row"> 
      {{--=========== include left panel = main content page=================== --}}  
        <!-- left panel   -->
        <div class="col-xs-12 col-sm-9 col-md-9 col-lg-9"> 
          @yield('NoiDung') 
        </div> <!-- /left panel -->
       {{--=========== include right-panel=================== --}}
        <!-- right penel -->
        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
          @include('front-end.modules.right-hotnews')   
          @include('front-end.modules.right-fanpage')   
          @include('front-end.modules.right-ads')    
        </div> <!-- /right panel      --> 
      </div>
      <!-- /row -->
    </div> <!-- / container /main content-->
@include ('front-end/layouts/footer')
    