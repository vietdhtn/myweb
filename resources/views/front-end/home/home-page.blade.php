@extends('front-end.layouts.master')
@section('NoiDung')
  {{-- hot news main page --}}
      <div class="panel panel-success">
        <div class="panel-heading">
          <h3 class="panel-title"> Tin tức mới <span class="glyphicon glyphicon-hand-right"></span> 
            <marquee direction="left" scrolldelay ="100">
              <a href="{!!url('tin-tuc/chi-tiet/'.$hot[0]['id'].'-'.$hot[0]['n_slug'])!!}" title=""> <b class="button"><span class="glyphicon glyphicon-hand-right"></span></b> 
              @foreach($hot as $item)
                {!!$item['n_title']!!}
              @endforeach
              </a> <b class="button"><span class="glyphicon glyphicon-hand-left"></span></b> </marquee> 
          </h3>
        </div>
        <div class="panel-body">
          <div class="row">
          <!-- hot new content -->
            <div class="col-lg-6">
              <img src="{!!url('public/uploads/news/'.$hot[0]['image'])!!}" alt="" height="200" width="350">
              <h3 class="title-h3"><a href="{!!url('tin-tuc/chi-tiet/'.$hot[0]['id'].'-'.$hot[0]['n_slug'])!!}" title="">{!!$hot[0]['n_title']!!}</a></h3>
              <p class="summary-content">
                {!!$hot[0]['intro']!!}
              </p>
            </div>
            <div class="col-lg-6 no-padding">
              <div class="row">
                <div class="col-lg-12 ">
                  <h4><a href="{!!url('tin-tuc/chi-tiet/'.$hot_1[1]['id'].'-'.$hot_1[1]['n_slug'])!!}" title="">{!!$hot_1[1]['n_title']!!}</a></h4>
                  <div class="col-lg-9">
                    <p class="sum">{!!$hot_1[1]['intro']!!}</p>
                  </div>
                  <div class="col-lg-3">
                    <a href="{!!url('tin-tuc/chi-tiet/'.$hot_1[1]['id'].'-'.$hot_1[1]['n_slug'])!!}" title=""><img src="{!!url('public/uploads/news/'.$hot_1[1]['image'])!!}" alt="" width="80" height="80" style="padding: 0;"></a>
                  </div>
                </div>
              </div>
              @foreach($hot_1 as $row)
                <ul class="new-cat no-padding">
                <li><a href="{!!url('tin-tuc/chi-tiet/'.$row['id'].'-'.$row['n_slug'])!!}">{!!$row['n_title']!!}</a></li>             
              </ul>
              @endforeach              
            </div>
          </div>
        </div>
      </div>   {{-- /hot news main page --}}
      {{-- include tab category --}}
      @include('front-end.modules.right-news-web-tab') 
      @include('front-end.modules.right-download-tab')   
      @include('front-end.modules.right-thuthuat-tab')           

@endsection