<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <div class="form-body">
            <ul class="nav nav-tabs final-login">
                <li class="active"><a data-toggle="tab" href="#sectionA">Đăng nhập</a></li>
                <li><a data-toggle="tab" href="#sectionB">Đăng ký</a></li>
            </ul>
            <div class="tab-content">
                <div id="sectionA" class="tab-pane fade in active">
                <div class="innter-form">
                    <form class="sa-innate-form" method="post">
                        <label>Địa Chỉ Email</label>
                            <input type="text" name="username" required placeholder="Ex: mywebsp@myweb.com">
                        <label>Mật Khẩu</label>
                            <input type="password" name="password" required placeholder="required">
                        <button type="submit">Đăng Nhập</button>
                        <a href="">Quên mật khẩu?</a>
                    </form>
                </div>
                <div class="social-login">
                    <p>- - - - - - - - - - - - -Hoặc Đăng nhập bằng - - - - - - - - - - - - - </p>
                    <ul> 
                        <li><a href=""><i class="fa fa-facebook"></i> Facebook</a></li>
                        <li><a href=""><i class="fa fa-google-plus"></i> Google+</a></li>                                             
                    </ul>
                </div>
                    <div class="clearfix"></div>
                </div>
                <div id="sectionB" class="tab-pane fade">
                    <div class="innter-form">
                    <form class="sa-innate-form" method="post">
                        <label>Họ Tên</label>
                            <input type="text" name="username" required>
                        <label>Địa Chỉ Email</label>
                            <input type="email" name="email" required>
                        <label>Mật Khẩu</label>
                            <input type="password" name="password" required>
                        <label>Xác Nhận Mật Khẩu</label>
                            <input type="password" name="rpassword" required>
                        <button type="submit">Đăng Ký</button>
                        <div class="social-login">
                            <p style="color:#16a085;">Bằng cách nhấn vào nút bây giờ, bạn đồng ý với thỏa thuận người bạn dùng, Chính sách bảo mật, và chính sách Cookie.</p>
                            <ul>
                                <li><a href=""><i class="fa fa-facebook"></i> Facebook</a></li>
                                <li><a href=""><i class="fa fa-google-plus"></i> Google+</a></li>                                
                            </ul>
                        </div>
                        <p></p>
                    </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>

