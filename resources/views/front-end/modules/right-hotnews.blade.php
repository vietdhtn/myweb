
<!-- panel inffo 1 -->
<div class="panel panel-info"> 
 <ul class="nav nav-tabs">
    <li class="active"><a href="#1" data-toggle="tab">Tin HOT</a></li>
    <li><a href="#2" data-toggle="tab">Tin Mới</a></li>
  </ul>    
  <div class="panel-body"> 
    <div class="tab-content ">
      <div class="tab-pane active" id="1">
        <ul class="li-menu-tab">
          <div class="row">
          <?php $news = App\News::select('*')->where('status','=','1')->orderBy('created_at', 'DESC')->limit(6)->get()->toArray(); ?>
          @foreach($news as $row)
            <div class="col-lg-12 no-padding">
              <div class="col-lg-3">
                <a href="{!!url('tin-tuc/chi-tiet/'.$row['id'].'-'.$row['n_slug'])!!}" title=""><img src="{!!url('public/uploads/news/'.$row['image'])!!}" alt="" width="44" height="55"> </a>
              </div>
              <div class="col-lg-9 no-padding">
               <a href="{!!url('tin-tuc/chi-tiet/'.$row['id'].'-'.$row['n_slug'])!!}" title="">{!!$row['n_title']!!}</a>
              </div>
            </div>
          @endforeach            
          </div>
        </ul>
      </div>
      <div class="tab-pane" id="2">
        <ul class="li-menu-tab">
          <div class="row">
            <?php $news = App\News::select('*')->where('status','=','1')->orderBy('created_at', 'ASC')->limit(6)->get()->toArray(); ?>
              @foreach($news as $row)
                <div class="col-lg-12 no-padding">
                  <div class="col-lg-3">
                    <a href="{!!url('tin-tuc/chi-tiet/'.$row['id'].'-'.$row['n_slug'])!!}" title=""><img src="{!!url('public/uploads/news/'.$row['image'])!!}" alt="" width="44" height="55"> </a>
                  </div>
                  <div class="col-lg-9 no-padding">
                   <a href="{!!url('tin-tuc/chi-tiet/'.$row['id'].'-'.$row['n_slug'])!!}" title="">{!!$row['n_title']!!}</a>
                  </div>
                </div>
              @endforeach      
          </div>
        </ul>
      </div>
    </div> <!-- /tab content -->
  </div>  <!-- /panel boody -->
</div>