    <!-- main navbar -->
    <nav class="navbar navbar-default navbar-top" role="navigation" id="main-Nav" style="background-color: #16a085;">
      <div class="container">  
        <div class="row">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-mav-top">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand hidden-lg hidden-md hidden-sm" href="#" style="color: #FFFFFF;">My Website - Menu</a>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="main-mav-top">
            <ul class="nav navbar-nav">
              <li> <a href="{!!url('')!!}" title="" style="color: #FFFFFF;background-color: #2c3e50;"><b class="glyphicon glyphicon-home"></b> Trang chủ </a> </li>
              <?php $cate = App\Category::select('*')->where('status','=','1')->get()->toArray(); ?>
                @foreach ($cate as $item)
                  @if ($item["parent_id"] == 0)
                  <li class="dropdown ul-sub">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{!! $item["c_name"]!!} <b class="caret"></b></a>
                    <?php 
                       subMenu ($cate,$item["id"]);
                    ?>
                    </li>
                  @endif                                
              @endforeach
            </ul>
          </div><!-- /.navbar-collapse -->
        </div>
      </div>
    </nav>
 <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-top" role="navigation" id="fixNav" style="background-color: #16a085;">
      <div class="container">  
        <div class="row">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-mav-top-fixed">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand hidden-lg hidden-md hidden-sm" href="#" style="color: #FFFFFF;">My Website - Menu</a>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="main-mav-top-fixed" style="background-color: #16a085; text-align: center;">
            <ul class="nav navbar-nav">
              <li> <a href="{!!url('')!!}" title="" style="color: #FFFFFF;background-color: #2c3e50;"><b class="glyphicon glyphicon-home"></b> Trang chủ </a> </li>
              <?php $cate = App\Category::select('*')->where('status','=','1')->get()->toArray(); ?>
                @foreach ($cate as $item)
                  @if ($item["parent_id"] == 0)
                  <li class="dropdown ul-sub">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{!! $item["c_name"]!!} <b class="caret"></b></a>
                    <?php 
                       subMenu ($cate,$item["id"]);
                    ?>
                    </li>
                  @endif                                
              @endforeach
              <li> <a href="{!!url('/tien-ich/home')!!}" title="" ><b class="glyphicon glyphicon-heart"></b> Tiện Ích</a> </li>
            </ul>
            <ul class="nav navbar-nav navbar-right ul-sub">
               <li ><a href="" data-toggle="modal" data-target="#myModal" style="color: blue;">Đăng nhập</a></li> 
               <li></li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div>
      </div>
    </nav>