<!--  penel  2 tab page-->
<div class="panel panel-info"> 
 <ul class="nav nav-tabs">
    <li class="active"><a href="#hosting" data-toggle="tab">Hosting - Doman</a></li>
    <li><a href="#web" data-toggle="tab"> Dịch Vụ Thiết Kế Web</a></li>    
    <li><a href="#tinth" data-toggle="tab">Thông Tin Tổng Hợp </a></li>
    <li><a href="#tincn" data-toggle="tab">Chuyên Tin công nghệ </a></li>
  </ul>    
  <div class="panel-body"> 
    <div class="tab-content ">
      <div class="tab-pane active" id="hosting">
        <ul class="li-menu-tab">
          <div class="row">
          <?php $hosting = App\News::select('*')->where('status','=','1')->where('c_id','=','21')->orderBy('created_at', 'ASC')->limit(6)->get()->toArray(); ?>
            @foreach($hosting as $row)
            <div class="col-lg-12 no-padding">
              <div class="col-lg-2">
                <a href="{!!url('dich-vu/chi-tiet/'.$row['id'].'-'.$row['n_slug'])!!}" title=""><img src="{!!url('public/uploads/news/'.$row['image'])!!}" alt="" width="100" height="100"> </a>
              </div>
              <div class="col-lg-10 no-padding">
                <h4><a href="{!!url('dich-vu/chi-tiet/'.$row['id'].'-'.$row['n_slug'])!!}" title="">{!!$row['n_title']!!}</a></h4>
                <p> 
                  {!!$row['intro']!!}
                </p>
              </div>
            </div>
            @endforeach            
          </div>
        </ul>
      </div>
      <div class="tab-pane" id="web">
        <ul class="li-menu-tab">
          <div class="row">
            <?php $hosting = App\News::select('*')->where('status','=','1')->where('c_id','=','21')->orderBy('created_at', 'ASC')->limit(6)->get()->toArray(); ?>
            @foreach($hosting as $row)
            <div class="col-lg-12 no-padding">
              <div class="col-lg-2">
                <a href="{!!url('dich-vu/chi-tiet/'.$row['id'].'-'.$row['n_slug'])!!}" title=""><img src="{!!url('public/uploads/news/'.$row['image'])!!}" alt="" width="100" height="100"> </a>
              </div>
              <div class="col-lg-10 no-padding">
                <h4><a href="{!!url('dich-vu/chi-tiet/'.$row['id'].'-'.$row['n_slug'])!!}" title="">{!!$row['n_title']!!}</a></h4>
                <p> 
                  {!!$row['intro']!!}
                </p>
              </div>
            </div>
            @endforeach            
          </div>
        </ul>
      </div>
      <div class="tab-pane" id="tinth">
        <ul class="li-menu-tab">
          <div class="row">
            <?php $hosting = App\News::select('*')->where('status','=','1')->where('c_id','=','22')->orderBy('created_at', 'ASC')->limit(6)->get()->toArray(); ?>
            @foreach($hosting as $row)
            <div class="col-lg-12 no-padding">
              <div class="col-lg-2">
                <a href="{!!url('tin-tuc/chi-tiet/'.$row['id'].'-'.$row['n_slug'])!!}" title=""><img src="{!!url('public/uploads/news/'.$row['image'])!!}" alt="" width="100" height="100"> </a>
              </div>
              <div class="col-lg-10 no-padding">
                <h4><a href="{!!url('tin-tuc/chi-tiet/'.$row['id'].'-'.$row['n_slug'])!!}" title="">{!!$row['n_title']!!}</a></h4>
                <p> 
                  {!!$row['intro']!!}
                </p>
              </div>
            </div>
            @endforeach
            
          </div>
        </ul>
      </div>
       <div class="tab-pane" id="tincn">
        <ul class="li-menu-tab">
          <div class="row">
            <?php $hosting = App\News::select('*')->where('status','=','1')->where('c_id','=','22')->orderBy('created_at', 'ASC')->limit(6)->get()->toArray(); ?>
            @foreach($hosting as $row)
            <div class="col-lg-12 no-padding">
              <div class="col-lg-2">
                <a href="{!!url('tin-tuc/chi-tiet/'.$row['id'].'-'.$row['n_slug'])!!}" title=""><img src="{!!url('public/uploads/news/'.$row['image'])!!}" alt="" width="100" height="100"> </a>
              </div>
              <div class="col-lg-10 no-padding">
                <h4><a href="{!!url('tin-tuc/chi-tiet/'.$row['id'].'-'.$row['n_slug'])!!}" title="">{!!$row['n_title']!!}</a></h4>
                <p> 
                  {!!$row['intro']!!}
                </p>
              </div>
            </div>
            @endforeach            
          </div>
        </ul>
      </div>
    </div> <!-- /tab content -->
  </div>  <!-- /panel boody -->
</div><!-- /panel info -->