<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('users')->insert([
            'name' => str_random(3).' '.str_random(3),
            'email' => 'admin'.'@gmail.com',
            'password' => bcrypt('123'),
            'status' => '1',
            'avata' => '/',
            'level' => '10',
        ]);
    }
}
