<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
// ============================font-end router=========================================== 
// index
Route::get('/', ['as'  => 'index', 'uses' =>'HomeController@index']);
Route::get('{parent_category}/{selected_category}',['as' =>'category','uses' => 'HomeController@category']);
// chi tiet ban tin
Route::get('{parent_category}/{selected_category}/{id}-{slug}',['as' =>'detail','uses' => 'HomeController@detail']);


//=============================== back-end ===================================
// --------------------------------login - log out ----------------------------------
Route::get('v_login', ['as'  => 'getlogin', 'uses' =>'LoginController@getlogin']);
Route::post('v_login', ['as' => 'postlogin','uses'=>'LoginController@postlogin']);
Route::get('logout', ['as'    => 'getlogout','uses' => 'LoginController@getlogout']);
Route::group(['middleware' => 'auth'], function () {
	// admin main pages
    Route::group(['prefix' => 'v_admin'], function () {
    	Route::get('/', ['as'  => 'home', 'uses' =>'AdminController@main']);
		// category area
		Route::group(['prefix' => 'category'], function () {
			Route::get('list', ['as'  => 'listCate', 'uses' =>'CategoryController@list']);

	    	Route::get('add', ['as'  => 'getAddCate', 'uses' =>'CategoryController@getaddcate']);
	    	Route::post('add', ['as'  => 'postAddCate', 'uses' =>'CategoryController@postaddcate']);

	    	Route::get('del/{id}',['as'   =>'getDelCate','uses' => 'CategoryController@delcate'])->where('id','[0-9]+');  	

	    	Route::get('edit/{id}',['as'  =>'getEditCate','uses' => 'CategoryController@getedit'])->where('id','[0-9]+');
           	Route::post('edit/{id}',['as' =>'postEditCate','uses' => 'CategoryController@postedit'])->where('id','[0-9]+');
		});
		Route::group(['prefix' => 'news'], function () {
			// list category
			Route::get('list', ['as'  => 'listNews', 'uses' =>'NewsController@list']);

	    	Route::get('add', ['as'  => 'getAddNews', 'uses' =>'NewsController@getaddnews']);
	    	Route::post('add', ['as'  => 'postAddNews', 'uses' =>'NewsController@postaddnews']);

	    	Route::get('del/{id}',['as'   =>'getDelNews','uses' => 'NewsController@delnews'])->where('id','[0-9]+');  	

	    	Route::get('edit/{id}',['as'  =>'getEditNews','uses' => 'NewsController@getedit'])->where('id','[0-9]+');
           	Route::post('edit/{id}',['as' =>'postEditNews','uses' => 'NewsController@postedit'])->where('id','[0-9]+');
		});
		Route::group(['prefix' => 'user'], function () {
			// list category
			Route::get('list', ['as'  => 'listUser', 'uses' =>'NewsController@list']);

	    	Route::get('add', ['as'  => 'getAddUser', 'uses' =>'NewsController@getadduser']);
	    	Route::post('add', ['as'  => 'postAddUser', 'uses' =>'NewsController@postadduser']);

	    	Route::get('del/{id}',['as'   =>'getDelUser','uses' => 'NewsController@deluser'])->where('id','[0-9]+');  	

	    	Route::get('edit/{id}',['as'  =>'getEditUser','uses' => 'NewsController@getedit'])->where('id','[0-9]+');
           	Route::post('edit/{id}',['as' =>'postEditUser','uses' => 'NewsController@postedit'])->where('id','[0-9]+');
		});
		Route::group(['prefix' => 'banner'], function () {
			// list category
			Route::get('list', ['as'  => 'listBaner', 'uses' =>'NewsController@list']);

	    	Route::get('add', ['as'  => 'getAddBaner', 'uses' =>'NewsController@getaddbanner']);
	    	Route::post('add', ['as'  => 'postAddBaner', 'uses' =>'NewsController@postaddbanner']);

	    	Route::get('del/{id}',['as'   =>'getDelBaner','uses' => 'NewsController@delbanner'])->where('id','[0-9]+');  	

	    	Route::get('edit/{id}',['as'  =>'getEditBaner','uses' => 'NewsController@getedit'])->where('id','[0-9]+');
           	Route::post('edit/{id}',['as' =>'postEditBaner','uses' => 'NewsController@postedit'])->where('id','[0-9]+');
		});
	});
});
