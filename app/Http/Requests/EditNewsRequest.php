<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return True;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sltCate'   => 'required',
            'txtTitle'  => 'required',
            'txtAuthor' =>'required',
            'txtIntro'  => 'required',
            'txtFull'   => 'required'
        ];
    }
    public function messages()
    {
        return [
            'sltCate.required'   => '- Vui lòng chọn danh mục cho bản tin ',
            'txtTitle.required'  => '- Vui lòng nhập tiêu đề bài viết ',
            'txtAuthor.required' => '- Vui lòng nhập tên tác giả ',
            'txtIntro.required'  => '- Vui lòng nhập tóm tắt bản tin ',
            'txtFull.required'   => '- Vui lòng nhập nội dung bản tin '
        ];
    }
}
