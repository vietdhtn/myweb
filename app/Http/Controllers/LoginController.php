<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\Requests\LoginRequest;
use App\User;

class LoginController extends Controller
{
    public function getlogin()
    {
        if (!Auth::check()) {
           return view('back-end.login.login');
        } else {
            return redirect('v_admin');
        }    	
    }
    public function postlogin(LoginRequest $request)
    {
    	if (Auth::attempt(['email' => $request->txtEmail, 'password' =>  $request->txtPass,'status' => '1'])) {
             return redirect('v_admin');
        } else{
        	return redirect('v_login');
        }
    }
    public function getlogout()
    {
        Auth::logout();
        Return redirect()->Route('getlogin');
    }
}
