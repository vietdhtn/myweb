<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Category;
use App\Http\Requests\AddCategoryRequest;
use App\Http\Requests\EditCategoryRequest;
use DB,datetime;

class CategoryController extends Controller
{
    public function list()
    {
    	$data= Category::select('*')->get()->toArray();
    	return view ('back-end.category.list',['data'=>$data]);
    }
    public function getaddcate()    {
    	$data= Category::select('*')->get()->toArray();
    	return view ('back-end.category.add',['data'=>$data]);
    }
    public function postaddcate(AddCategoryRequest $request )
    {
		$category             = new Category;
		$category->c_name       = $request->txtCateName;
		$category->c_slug       = str_slug($request->txtCateName,'-');
		$category->parent_id  = $request->sltCate;
		$category->status       = '1';
		$category->note       = $request->txtNote;
		$category->created_at = new DateTime;
		$category->save();
		return redirect()->route('listCate')
		->with(['flash_level'=>'result_msg','flash_massage'=>'Thêm thành công !']);
    }
    // --------------------------get del (xoa danh muc) ---------------------------
   public function delcate($id)
   {
      $parent_id = category::where('parent_id',$id)->count();
      if ($parent_id==0) {
         $category = category::find($id);
         $category->delete();
         return redirect()->route('listCate')
         ->with(['flash_level'=>'result_msg','flash_massage'=>'Xóa thành công !']);
      } else{
         echo '<script type="text/javascript">
                  alert("Không được phép xóa mục này !");                
                window.location = "';
                echo route('getaddcategory');
            echo '";
         </script>';
      }
   }
   // ----------------------------------get edit (----------------------------------------
   public function getedit($id)
   {
		$data                                        = category::findOrFail($id)->toArray();
		$cha                                         = category::select('*')->get()->toArray();
		return view('back-end.category.edit',['data' =>$data,'cha'=>$cha]);
   }
   // ----------------------------------post edit (sua danh muc)------------------------------------------
   public function postedit(EditCategoryRequest $request,$id)
   {
		$category             = category::find($id);
		$category->c_name     = $request->txtCateName;
		$category->c_slug     = str_slug($request->txtCateName,'-');
		$category->parent_id  = $request->sltCate;
		$category->status     = $request->sltStatus;
		$category->note       = $request->txtNote;
		$category->updated_at = new DateTime;
		$category->save();
		return redirect()->route('listCate')
		->with(['flash_level' =>'result_msg','flash_massage'=>'Sửa thành công !']);
   }
}
