<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\News;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AddNewsRequest;
use App\Http\Requests\EditNewsRequest;
use DateTime,File;

class NewsController extends Controller
{
    public function list()
    {
    	$_page = News::paginate(10);
        $data = News::select('*')->orderBy('created_at', 'DESC')->paginate(10)->toArray();
    	return view ('back-end.news.list',['data'=>$data,'page'=>$_page]);
    }
    public function getaddnews()
    {
        $cat = Category::select('*')->get()->toArray();
        return view('back-end.news.add',['danhmuc' => $cat]);
    }
    // ---------------them ban tin moi ---------------------
    public function postaddnews(AddNewsRequest $request)
    {
        $news                  = new News;
        $file                    = $request->file('newsImg');
        $news->n_title          = $request->txtTitle;       
        $news->author          = $request->txtAuthor;
        $news->n_slug = str_slug($request->txtTitle,'-');
        $news->intro          = $request->txtIntro;
        $news->full         = $request->txtFull;
        if (strlen($file) > 0) 
        	{
		        $filename                = time().'.'.$file->getClientOriginalName();
		        $destinationPath         = 'public/uploads/news/';
		        $file->move($destinationPath,$filename);
		        $news->image         = $filename;
        	}
        $news->c_id      = $request->sltCate;
        $news->status       = $request->rdoPublic;  
        $news->source       = $request->txtSource;          
        $news->user_id        =    Auth::user()->id;
        $news->created_at      = new DateTime;
        $news->save();
        return redirect()->route('listNews')
        ->with(['flash_level'    =>'result_msg','flash_massage'=>'Thêm thành công !']);
    }
    public function delnews($id)
    {
        $news = News::findOrFail($id);
        if (file_exists(public_path().'/uploads/news/'.$news->image)) {
            File::delete(public_path().'/uploads/news/'.$news->image);
        }
        $news->delete();
        return redirect()->route('listNews')->with(['flash_level' =>'result_msg','flash_massage'=>'Đã xóa thành công !']);
    }
     public function getedit($id)
    {
        $news                                 = News::findOrFail($id);
        $cat                                = Category::select('*')->get()->toArray();
        return view('back-end.news.edit',["data" => $news,'danhmuc' => $cat]);
    }
    public function postedit(EditNewsRequest $request, $id)
    {
        $news = News::findOrFail($id);
        $file = $request->file('newsImg');
        $news->n_title       =    $request->txtTitle;
        $news->n_slug       =  str_slug($request->txtTitle,"-");
        $news->author      =    $request->txtAuthor;
        $news->intro       =    $request->txtIntro;
        $news->full        =    $request->txtFull;
        if (strlen($file) > 0) {
        $fImageCurrent = $request->fImageCurrent;
        if (file_exists(public_path().'/public/uploads/news/'.$fImageCurrent)) {
        File::delete(public_path().'/public/uploads/news/'.$fImageCurrent);
        }
        
        $filename = time().'.'.$file->getClientOriginalName();
        $destinationPath = 'public/uploads/news/';
        $file->move($destinationPath,$filename);
        $news->image = $filename;
        }
        $news->source       = $request->txtSource;    
        $news->status      =    $request->rdoPublic;
        $news->c_id =    $request->sltCate;
        $news->user_id    =    Auth::user()->id;
        $news->updated_at  =    new DateTime();
        $news->save();
        return redirect()->route('listNews')->with(['flash_level' =>'result_msg','flash_massage'=>'Đã sửa thành công !']);
    }
}
