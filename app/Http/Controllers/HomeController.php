<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\News;
use App\Category;

class HomeController extends Controller
{
   public function index()
   {
      $hot = News::select('*')->where('status','=','1')->orderBy('created_at', 'DESC')->limit(1)->get()->toArray();
      $hot_1 = News::select('*')->where('status','=','1')->orderBy('created_at', 'DESC')->limit(6)->get()->toArray();
   	return view('front-end.home.home-page',['hot'=>$hot,'hot_1'=>$hot_1]);
   }
   // categoty
   public function category($parent_category,$selected_category)
   {
      $cat = Category::select('*')->where('c_slug','=',$parent_category)->get()->toArray();
      $scat = Category::select('*')->where('c_slug','=',$selected_category)->get()->toArray();
   	if ($cat && $scat ) {
            $data = News::select('*')->where('c_id','=',$scat[0]['id'])->limit(1)->get()->toArray();
            $data_cat = News::select('*')->where('c_id','=',$scat[0]['id'])->orderBy('created_at', 'DESC')->limit(8)->get()->toArray();
            // print_r($data);
            // exit();
   			return view('front-end.category.web',['cat'=>$cat[0]['c_name'],'cat_slug'=>$cat[0]['c_slug'],'scat'=>$scat[0]['c_name'],'data'=>$data,'data_cat'=>$data_cat]);
                       
   		}      
      else {
         return view('errors.503');
      }
   }
   // detail content
   public function detail($cat,$scat,$id,$slug)
   {
      $data = News::select('*')->where('status','=','1')->where('id','=',$id)->get()->toArray();
      $c = Category::select('c_name')->where('c_slug','=',$cat)->get()->toArray();
      return view('front-end.detail.news-detail',['data'=>$data,'cat'=>$c]);
   }
}
