<?php 
	function MenuMulti($data,$parent_id =0,$str='---| ',$select=0)
	{
		foreach ($data as $val) {
			$id = $val["id"];
			$ten= $val["c_name"];
			if ($val['parent_id'] == $parent_id) {
				if ($select!=0 && ($id == $select)) {
					echo '<option value="'.$id.'" selected>'.$str." ".$ten.'</option>';
				}	else {
					echo '<option value="'.$id.'">'.$str." ".$ten.'</option>';
				}			
				MenuMulti($data,$id,$str.'---|');
			}			
		}
	}
	function listcate ($data,$parent_id =0,$str="")
	{
		foreach ($data as $val) {
			$id = $val["id"];
			$ten= $val["c_name"];
			$status= $val["status"];
			if ($val['parent_id'] == $parent_id) {
				echo '<tr>';
				if ($str =="") {
						echo '<td class="list_td alignleft"><strong>'.$str.$ten.'</strong></td>';
					} else {
						echo '<td class="list_td alignleft">'.$str.$ten.'</td>';
					}
				if ($status == 1) {
						echo '<td style="width:90px;text-align:center; "><strong><span class="glyphicon glyphicon-ok" style="color:green;"></span> </strong></td>';
					} else {
						echo '<td  style="width:90px; text-align:center;"><span class="glyphicon glyphicon-remove" style="color:red;"></span> </td>';
					}	
			echo '<td class="list_td" style="text-align:center; width:200px;">
		            <a href="../category/edit/'.$id.'"><span class="glyphicon glyphicon-edit"></span> Edit </a>&nbsp;&nbsp;&nbsp;
		            <a href="../category/del/'.$id.'"onclick="return xacnhan(\'Xóa danh mục này ?\') "><span class="glyphicon glyphicon-remove"> Delete</span></a>
			      </td>    
			    </tr>';
			    listcate ($data,$id,$str." ---| ");
			}
		}		
	}
	function subMenu ($data,$id) 
	{	
		echo '<ul class="dropdown-menu ul-sub">';
			foreach ($data as $item) {
				$cate = App\Category::select('*')->where('id','=',$item["parent_id"])->where('status','=','1')->get()->toArray();
				if ($item["parent_id"] == $id) {
					echo '<li><a href="http://localhost/myweb/'.$cate[0]['c_slug'].'/'.$item["c_slug"].'">'.$item["c_name"].'</a></li>';	
					echo '<li class="divider"></li>';			
				}			
			}
		echo '</ul>';	
		
	}
?>